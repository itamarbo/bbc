// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCG3X5UZPbbS5jclbmOFFyAY-fjA1qPj_4",
    authDomain: "bbcexample-49db1.firebaseapp.com",
    databaseURL: "https://bbcexample-49db1.firebaseio.com",
    projectId: "bbcexample-49db1",
    storageBucket: "bbcexample-49db1.appspot.com",
    messagingSenderId: "872350367579",
    appId: "1:872350367579:web:7b6c60a81f414b2648647e",
    measurementId: "G-N5LHZSR9MY"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
