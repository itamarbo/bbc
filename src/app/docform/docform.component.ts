import { Router } from '@angular/router';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-docform',
  templateUrl: './docform.component.html',
  styleUrls: ['./docform.component.css']
})
export class DocformComponent implements OnInit {

  text:string;

  constructor(private classifyService:ClassifyService, private router:Router) { }

  ngOnInit() {
  }

  onSubmit(){
    this.classifyService.doc = this.text;
    this.router.navigate(['/classified'])
  }

}
