import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  public image:string[] = [];
  public path:string = "https://firebasestorage.googleapis.com/v0/b/bbcexample-49db1.appspot.com/o/";

  constructor() {
    this.image[0] = this.path + 'biz.JPG' + '?alt=media';
    this.image[1] = this.path + 'entermnt.JPG' + '?alt=media';
    this.image[2] = this.path + 'politics-icon.png' + '?alt=media';
    this.image[3] = this.path + 'sport.JPG' + '?alt=media';
    this.image[4] = this.path + 'tech.JPG' + '?alt=media';
   }
}
