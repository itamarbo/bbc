import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  myurl="https://jsonplaceholder.typicode.com/posts";

  getPosts(){
    return this.httpClient.get(this.myurl);
  }

  constructor(public httpClient:HttpClient) { }
}
