import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class AlbumsService {

  myUrl="https://jsonplaceholder.typicode.com/albums";

  getAlbums(){
    return this.httpClient.get(this.myUrl);
  }

  constructor(public httpClient:HttpClient) { }
}
