import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { RouterModule, Routes } from '@angular/router';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatExpansionModule } from '@angular/material/expansion';
import { BooksComponent } from './books/books.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material';
import { TempformComponent } from './tempform/tempform.component';
import { HttpClientModule } from '@angular/common/http';

import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule, AngularFireAuth } from '@angular/fire/auth';
import { BookformComponent } from './bookform/bookform.component';
import { AlbumsComponent } from './albums/albums.component';

import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { DocformComponent } from './docform/docform.component';
import { ClassifiedComponent } from './classified/classified.component';
import { PostsComponent } from './posts/posts.component';





const appRoutes: Routes = [
  { path: 'welcome', component: WelcomeComponent },
  { path: 'books', component: BooksComponent },
  { path: 'temperatures', component: TemperaturesComponent },
  { path: 'temperatures/:temp/:city', component: TemperaturesComponent },
  { path: 'temperatures/:city', component: TemperaturesComponent},
  { path: 'tempform', component: TempformComponent },
  { path: 'bookform', component: BookformComponent },
  { path: 'bookform/:id', component: BookformComponent },
  { path: 'albums', component: AlbumsComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'docform', component: DocformComponent },
  { path: 'classified', component: ClassifiedComponent },
  { path: 'posts', component: PostsComponent },
  { path: "",
    redirectTo: '/welcome',
    pathMatch: 'full'
  },
];


@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    NavComponent,
    BooksComponent,
    TemperaturesComponent,
    TempformComponent,
    BookformComponent,
    AlbumsComponent,
    SignupComponent,
    LoginComponent,
    DocformComponent,
    ClassifiedComponent,
    PostsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatCardModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    FormsModule,
    MatSelectModule,
    MatInputModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireStorageModule,
    AngularFireModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    AngularFirestoreModule,
    
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
  ],
  providers: [AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
