import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(public authService:AuthService, public router:Router) { }

  email:string;
  password:string;

  onSubmit(){
    this.authService.signup(this.email, this.password);
    this.router.navigate(['/books']);
  }

  ngOnInit() {
  }

}
