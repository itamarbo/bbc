import { AuthService } from './../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bookform',
  templateUrl: './bookform.component.html',
  styleUrls: ['./bookform.component.css']
})
export class BookformComponent implements OnInit {

  title:string;
  author:string;
  id:string;
  isEdit:boolean = false;
  buttonText:string = "Add Book";
  userId:string;

  constructor(private booksService:BooksService, private router:Router, private route:ActivatedRoute, private authService:AuthService) { }

  onSubmit(){
    //this.booksService.addBook(this.title, this.author);
    if(this.isEdit){
      this.booksService.updateBook(this.userId, this.id, this.title, this.author)
    }
    else{
      this.booksService.addBook(this.userId, this.title, this.author);
    }
    this.router.navigate(['/books']);
  }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
        
        //relevant only for update book
        if(this.id){
          this.isEdit = true;
          this.buttonText = "Update Book";
          this.booksService.getBook(this.id, this.userId).subscribe(
            book => {
              this.author = book.data().author;
              this.title = book.data().title;
            })
        }
      }
    )
  }

}
